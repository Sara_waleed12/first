import 'package:flutter/material.dart';
void main() {
  Color redColor = Color(0xffA71E27);
  runApp(
    MaterialApp(
      builder: (context,widget){
        return Directionality(textDirection: TextDirection.ltr, child: widget!);
      },
      home:Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          //twitter logo
          title: Text("ex24 Express"),
          centerTitle: false,
          elevation: 10,
          backgroundColor: redColor,
          // leading: Icon(Icons.arrow_back_rounded),
          actions: [
            Icon(Icons.search_rounded),
            SizedBox(width: 16,),
            Icon(Icons.shopping_cart),
            SizedBox(width: 16,),
          ],
        ),
        drawer: Drawer(
          backgroundColor: redColor,
        ),
        // floatingActionButton: FloatingActionButton(
        //   onPressed: (){},
        //   backgroundColor: Colors.purple,
        //   child: Icon(Icons.add,size: 35,),
        // ),
        // floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        bottomNavigationBar: ClipRRect(borderRadius: BorderRadius.only(topRight: Radius.circular(15), topLeft:  Radius.circular(15),),
          child: BottomNavigationBar(
            //for more than three items
            type: BottomNavigationBarType.fixed,
            backgroundColor: Colors.white,
            items: [
              BottomNavigationBarItem(
                  icon: Icon(Icons.home,size: 35,),
                  label: ""
              ),
              BottomNavigationBarItem(
                  icon: Icon(Icons.category,size: 35,),
                  label: ""
              ),
              BottomNavigationBarItem(
                  icon: Icon(Icons.favorite_border,size: 35,),
                  label: ""
              ),
            ],
          ),
        ),
      ),
    ),
  );
}
